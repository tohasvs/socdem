from flask import request
from flask.ext.api import FlaskAPI, status
import json

from socdem import SocDemAPI

app = FlaskAPI(__name__)

sex_model_filename = '/Volumes/TOSHIBA/Diplom/pipeline_tfid_log_model_test.sav'
age_model_filename = '/Volumes/TOSHIBA/Diplom/pipeline_tfid_log_model_test.sav'
socDemAPI = SocDemAPI(sex_model_filename, age_model_filename)


def success_status():
    return {
        'status': "ok"
    }


def error_status(message):
    return {
        'status': "error",
        'body': message
    }


@app.route("/saveUserHistory", methods=['GET', 'POST'])
def save_user_history_by_site():
    """
    save user's history by uid
    """
    if request.method == 'POST':
        uid = request.data.get('uid', -1)
        site_url_str_list = request.data.get('site_list', '')
        site_list = json.loads(site_url_str_list)
        if len(site_list) == 0:
            return error_status("Передан пустой список заголовков"), status.HTTP_400_BAD_REQUEST
        if uid == -1:
            return error_status("uid пустой"), status.HTTP_400_BAD_REQUEST
        socDemAPI.save_user_history_list(uid, site_list)
        return get_user_history_list(uid), status.HTTP_201_CREATED
    return {
        "message": "save user's history by uid"
    }


@app.route("/history/<int:uid>/", methods=['GET'])
def get_user_history_list(uid):
    """
    Get user's history by uid
    """
    return [history.site.site_title for history in socDemAPI.get_user_history_list(uid)]


@app.route("/socdem", methods=['GET'])
def get_user_sex_and_age():
    uid = request.args.get('uid', -1)
    if uid == -1:
        return error_status("uid пустой"), status.HTTP_400_BAD_REQUEST
    sex, age = socDemAPI.get_user_sex_and_age(uid)
    return {
        'sex': str(sex),
        'age': str(age)
    }


if __name__ == "__main__":
    app.run(debug=True, port=8052)
