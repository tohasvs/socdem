from db import UserHistoryDatabase
import requests
from bs4 import BeautifulSoup
from sklearn.externals import joblib


class SocDemAPI:
    def __init__(self, sex_filename, age_filename):
        self.db = UserHistoryDatabase()
        self.sex_model = joblib.load(sex_filename)
        self.age_model = joblib.load(age_filename)
        print("Model initialized")

    def load_title(self, site_url):
        """Private"""
        soup = BeautifulSoup(requests.get(site_url).text, "lxml")
        if soup.title is None:
            return None
        print(soup.title.string)
        return soup.title.string

    def get_site_model_by_site_url(self, site):
        """Private"""
        site_list = self.db.get_site_list_by_site_url(site['url'])
        if len(site_list) != 0:
            return site_list[0]

        title = site['title']
        if title is None or title == '':
            title = self.load_title(site['url'])
            if title is None:
                return None
        return self.db.save_site(site['url'], title)

    def save_user_history_list(self, user_id, site_list):
        for site in site_list:
            self.save_user_history(user_id, site)

    def save_user_history(self, user_id, site):
        """Public"""
        site = self.get_site_model_by_site_url(site)
        if site is not None:
            self.db.save_user_history(user_id, site)

    def get_user_history_list(self, user_id):
        """Public"""
        return self.db.get_user_history(user_id)

    def get_user_sex_and_age(self, user_id):
        """Public"""
        user_history = self.db.get_user_history(user_id)
        if len(user_history) == 0:
            return -1, -1
        title_str = ". ".join(list(map(lambda x: x.site.site_title, user_history)))
        sex = self.sex_model.predict([title_str])[0]
        age = self.age_model.predict([title_str])[0]
        return sex, age
