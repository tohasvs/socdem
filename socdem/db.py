from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import datetime

Base = declarative_base()


class Site(Base):
    __tablename__ = 'site'

    site_url = Column(String, primary_key=True)
    site_title = Column(String)

    def __repr__(self):
        return "<Site(site_url='%s', site_title='%s')>" % (self.site_url, self.site_title)


class UserHistory(Base):
    __tablename__ = 'user_history'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    created_time = Column(DateTime, default=datetime.datetime.utcnow)
    site_url = Column(String, ForeignKey('site.site_url'))
    site = relationship('Site', backref='site')

    def __repr__(self):
        return "<UserHistory(id='%d', user_id='%d', site_url='%s')>" % (self.id, self.user_id, self.site_url)


class UserHistoryDatabase:
    def __init__(self, database_name='socdem.db'):
        engine = create_engine('sqlite:///' + database_name, echo=False)
        Session = sessionmaker(bind=engine)
        self.session = Session()
        Base.metadata.create_all(engine)

    def save_site(self, site_url, site_title):
        site = Site(site_url=site_url, site_title=site_title)
        self.session.add(site)
        self.session.commit()
        return site

    def save_user_history(self, user_id, site):
        user_history = UserHistory(user_id=user_id, site=site)
        self.session.add(user_history)
        self.session.commit()

    def get_site_list_by_site_url(self, site_url):
        return self.session.query(Site).filter(Site.site_url == site_url).all()

    def get_user_history(self, user_id):
        return self.session.query(UserHistory).filter(UserHistory.user_id == user_id).all()
